
import com.swcguild.flooringmastery.dao.FlooringMasteryDAO;
import java.io.FileNotFoundException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FlooringMasteryDAOImplTest {

    FlooringMasteryDAO testObject = new FlooringMasteryDAO();

    public FlooringMasteryDAOImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLoadConfig() {
        Assert.assertTrue(testObject.loadConfig()); //should fail when in test mode
    }

    @Test
    public void testCreateNewOrderOneOrderNotEmpty() throws FileNotFoundException {
        testObject.loadSalesTax();
        testObject.loadFlooringData();
        testObject.createNewOrder("09099887", "Sol", "SC", "Wood", 100.0);
        Assert.assertTrue(testObject.getOrdersForDate("09099887").size() == 1);
    }

    @Test
    public void testCreateNewOrderOneOrderNotNull() throws FileNotFoundException {
        testObject.loadSalesTax();
        testObject.loadFlooringData();
        testObject.createNewOrder("09099887", "Sol", "SC", "Wood", 100.0);
        Assert.assertNotNull(testObject.getOrdersForDate("09099887"));
    }

    @Test
    public void testDisplayOrdersForDate() {
        Assert.assertTrue(testObject.getOrdersForDate("09099889").isEmpty());
    }
}
