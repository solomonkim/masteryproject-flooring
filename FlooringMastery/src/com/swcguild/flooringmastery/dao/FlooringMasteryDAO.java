package com.swcguild.flooringmastery.dao;

import com.swcguild.flooringmastery.dto.Material;
import com.swcguild.flooringmastery.dto.Order;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import org.apache.commons.io.FileUtils;

public class FlooringMasteryDAO {

    private HashMap<Integer, Order> allOrders = new HashMap<>();
    private static final String DELIMITER = ",";
    HashMap<String, Material> materialData = new HashMap<>();
    HashMap<String, Double> stateTax = new HashMap<>();
    Random r = new Random();
    private static final String FLOORING_FILE = "FlooringData.txt";
    private static final String STATE_TAX_FILE = "StateTax.txt";
    int orderNumber;
    String customerName;
    String stateAbbreviation;
    Double taxRate;
    String productType;
    Double area;
    Double matCostPerSquareFoot;
    Double labCostPerSquareFoot;
    Double matCost;
    Double labCost;
    Double tax;
    Double total;
    String mmddyyyy;

    public boolean loadConfig() {
        File configFile = new File("config.txt");
        try {
            Scanner configFileSc = new Scanner(new BufferedReader(new FileReader(configFile)));
            return (configFileSc.next().contains("prod"));
        } catch (FileNotFoundException ex) {
            return false;
        }
    }

    public void loadFiles() throws FileNotFoundException {
        File ordersFileDirectory = new File("Orders");
        for (File f : ordersFileDirectory.listFiles()) {
            if (f.getName().contains("~")) {
                continue;
            }
            Scanner sc = new Scanner(new BufferedReader(new FileReader(f)));
            while (sc.hasNextLine()) {
                String recordLine = sc.nextLine();
                String[] recordProperties = recordLine.split(DELIMITER);
                for (int i = 0; i < recordProperties.length; i++) {
                    recordProperties[i] = recordProperties[i].replace("<COMMA>", ",");
                }
                if (recordProperties.length != 13) {
                    continue;
                }
                Order newOrder = new Order();
                newOrder.setArea(Double.parseDouble(recordProperties[5]));
                newOrder.setMatCostPerSquareFoot(Double.parseDouble(recordProperties[6]));
                newOrder.setCustomerName(recordProperties[1]);
                newOrder.setLabCost(Double.parseDouble(recordProperties[9]));
                newOrder.setLabCostPerSquareFoot(Double.parseDouble(recordProperties[7]));
                newOrder.setMatCost(Double.parseDouble(recordProperties[8]));
                newOrder.setMmddyyyy(recordProperties[12]);
                newOrder.setOrderNumber(Integer.parseInt(recordProperties[0]));
                newOrder.setProductType(recordProperties[4]);
                newOrder.setState(recordProperties[2]);
                newOrder.setTax(Double.parseDouble(recordProperties[10]));
                newOrder.setTaxRate(Double.parseDouble(recordProperties[3]));
                newOrder.setTotal(Double.parseDouble(recordProperties[11]));
                allOrders.put(newOrder.getOrderNumber(), newOrder);
            }
            sc.close();
        }
    }

    public void loadFlooringData() throws FileNotFoundException {
        Scanner flooringFileSc = new Scanner(new BufferedReader(new FileReader(FLOORING_FILE)));
        while (flooringFileSc.hasNextLine()) {
            String recordLine = flooringFileSc.nextLine();
            String[] recordLineProperties = recordLine.split(DELIMITER);
            if (recordLineProperties.length != 3) {
                continue;
            }
            String productType = recordLineProperties[0];
            Double matCost = Double.parseDouble(recordLineProperties[1]);
            Double labCost = Double.parseDouble(recordLineProperties[2]);
            Material matToLoad = new Material(productType, matCost, labCost);
            materialData.put(productType, matToLoad);
        }
        flooringFileSc.close();
    }

    public void loadSalesTax() throws FileNotFoundException {
        Scanner loadSalesTaxSc = new Scanner(new BufferedReader(new FileReader(STATE_TAX_FILE)));
        while (loadSalesTaxSc.hasNextLine()) {
            String recordLine = loadSalesTaxSc.nextLine();
            String[] recordLineProperties = recordLine.split(DELIMITER);
            if (recordLineProperties.length != 2) {
                continue;
            }
            String stateName = recordLineProperties[0];
            Double taxRate = Double.parseDouble(recordLineProperties[1]);
            stateTax.put(stateName, taxRate);
        }
        loadSalesTaxSc.close();
    }

    public void createNewOrder(String mmddyyyy, String customerName, String stateAbbreviation, String productType, Double area) {
        this.mmddyyyy = mmddyyyy;
        this.customerName = customerName;
        this.stateAbbreviation = stateAbbreviation;
        this.productType = productType;
        this.area = area;
        taxRate = stateTax.get(stateAbbreviation) / 100;
        matCostPerSquareFoot = materialData.get(productType).getMatRate();
        labCostPerSquareFoot = materialData.get(productType).getLabRate();
        matCost = matCostPerSquareFoot * area;
        labCost = labCostPerSquareFoot * area;
        tax = taxRate * (matCost + labCost);
        total = (matCost + labCost + tax);
        orderNumber = orderNumber();
        Order newOrder = new Order(orderNumber, customerName, stateAbbreviation, taxRate, productType, area, matCostPerSquareFoot, labCostPerSquareFoot, matCost, labCost, tax, total, mmddyyyy);
        allOrders.put(newOrder.getOrderNumber(), newOrder);
    }

    public void writeFiles() throws IOException {
        HashSet<String> orderDates = new HashSet<>();
        Collection<Order> orderCollection = allOrders.values();
        File ordersFileDirectory = new File("Orders");
        for (Order o : orderCollection) {
            orderDates.add(o.getMmddyyyy());
        }
        FileUtils.cleanDirectory(ordersFileDirectory);
        for (String s : orderDates) {
            String fileName = "Orders/Orders_" + s + ".txt";
            PrintWriter fileMaker = new PrintWriter(new FileWriter(fileName));
            String orderToWriteToFile;
            for (Order o : orderCollection) {
                if (s.equals(o.getMmddyyyy())) {
                    orderToWriteToFile = o.getOrderNumber() + DELIMITER + o.getCustomerName().replace(",", "<COMMA>") + DELIMITER + o.getState() + DELIMITER + o.getTaxRate() + DELIMITER + o.getProductType() + DELIMITER + o.getArea() + DELIMITER + o.getMatCostPerSquareFoot() + DELIMITER + o.getLabCostPerSquareFoot() + DELIMITER + o.getMatCost() + DELIMITER + o.getLabCost() + DELIMITER + o.getTax() + DELIMITER + o.getTotal() + DELIMITER + o.getMmddyyyy();
                    fileMaker.println(orderToWriteToFile);
                }
            }
            fileMaker.flush();
            fileMaker.close();
        }
    }

    public ArrayList<Order> getOrdersForDate(String date) {
        ArrayList<Order> ordersForDate = new ArrayList<>();
        for (Order o : allOrders.values()) {
            if (date.equals(o.getMmddyyyy())) {
                ordersForDate.add(o);
            }
        }
        return ordersForDate;
    }

    public Order getOrderByOrderNumber(int orderNumber) {
        return allOrders.get(orderNumber);
    }

    public void deleteOrder(int orderNumber) { //return deleted Order?
        allOrders.remove(orderNumber);
    }

    public int orderNumber() {
        int highestOrderNumber = 101;
        for (Order o : allOrders.values()) {
            if (o.getOrderNumber() >= highestOrderNumber) {
                highestOrderNumber = o.getOrderNumber() + 1;
            }
        }
        return highestOrderNumber;
    }
}
