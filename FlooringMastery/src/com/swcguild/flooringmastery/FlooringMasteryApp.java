package com.swcguild.flooringmastery;

import com.swcguild.flooringmastery.ops.FlooringMasteryController;
import java.io.IOException;

public class FlooringMasteryApp {

    public static void main(String[] args) throws IOException {
        FlooringMasteryController controller = new FlooringMasteryController();
        controller.run();
    }
}
