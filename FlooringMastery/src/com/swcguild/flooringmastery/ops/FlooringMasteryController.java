package com.swcguild.flooringmastery.ops;

import com.swcguild.flooringmastery.dao.FlooringMasteryDAO;
import com.swcguild.flooringmastery.dto.Order;
import com.swcguild.flooringmastery.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class FlooringMasteryController {

    private ConsoleIO console = new ConsoleIO();
    FlooringMasteryDAO dao = new FlooringMasteryDAO();
    boolean isProdMode = dao.loadConfig();

    public void run() throws IOException {
        loadOrderData();
        loadFlooringData();
        loadSalesTaxData();
        boolean keepRunning = true;
        int menuChoice;
        while (keepRunning) {
            printMenu();
            menuChoice = console.readInt("enter selection:", 1, 6);
            switch (menuChoice) {
                case 1:
                    displayOrdersForDate();
                    break;
                case 2:
                    createNewOrder();
                    break;
                case 3:
                    editOrder();
                    break;
                case 4:
                    deleteOrder();
                    break;
                case 5:
                    if (isProdMode) {
                        writeFiles();
                        console.print("All changes have been saved.");
                    } else {
                        console.print("The program is in test mode. Changes cannot be saved.");
                    }
                    break;
                case 6:
                    if (isProdMode) {
                        writeFiles();
                        console.print("All changes have been saved. Exiting program...");
                    } else {
                        console.print("The program is in test mode. Changes will not be saved. Exiting program...");
                    }
                    keepRunning = false;
                    break;
                default:
                    console.print("Invalid selection. Changes will not be saved. Exiting program...");
                    keepRunning = false;
                    break;
            }
        }
    }

    private void loadOrderData() throws FileNotFoundException {
        if (isProdMode) {
            dao.loadConfig();
        }
    }

    private void loadFlooringData() throws FileNotFoundException {
        dao.loadFlooringData();
    }

    private void loadSalesTaxData() throws FileNotFoundException {
        dao.loadSalesTax();
    }

    private void printMenu() {
        console.print("\nMain Menu");
        console.print("1. display an order");
        console.print("2. create a new order");
        console.print("3. edit an order");
        console.print("4. delete an order");
        console.print("5. save");
        console.print("6. exit");
    }

    private void createNewOrder() {
        console.print("***new order***");
        String mmddyyyy;
        mmddyyyy = console.readString("date (mmddyyyy):");
        String customerName = console.readString("name:");
        String state = console.readString("state (two-letter abbrevation):");
        String productType = console.readString("product type (\"Carpet\", \"Laminate\", \"Tile\" or \"Wood\"):");
        Double area = console.readDouble("area (square feet): ");
        int confirmOrder = console.readInt("***new order summary***\n"
                + "date: " + mmddyyyy + "\n"
                + "name: " + customerName + "\n"
                + "state: " + state + "\n"
                + "product type: " + productType + "\n"
                + "area: " + area + "\n"
                + "1. confirm--add order\n"
                + "2. cancel--return to main menu");
        if (confirmOrder == 1) {
            dao.createNewOrder(mmddyyyy, customerName, state, productType, area);
        } else {
            console.print("Order cancelled. Returning to main menu...");
        }
    }

    private void displayOrdersForDate() {
        String dateOfOrdersToDisplay = console.readString("Enter the date of the order you want to view: (mmddyyyy):");
        ArrayList<Order> ordersForDate = dao.getOrdersForDate(dateOfOrdersToDisplay);
        if (ordersForDate.isEmpty()) {
            console.print("There are no orders for that date. Returning to main menu...");
        } else {
            console.print("***orders for " + dateOfOrdersToDisplay + "***");
            console.print("order#\tcustomer");

            for (Order o : ordersForDate) {
                console.print(o.getOrderNumber() + "\t" + o.getCustomerName());
            }
            int orderNumberOfOrderToDisplay = console.readInt("Enter the order number for detailed information: "); //add check if order number exists
            Order singleOrderToDisplay = dao.getOrderByOrderNumber(orderNumberOfOrderToDisplay);
            if (singleOrderToDisplay == null) {
                console.print("There is no order associated with that order number. Returning to main menu...");
            } else {
                console.print("order #: " + singleOrderToDisplay.getOrderNumber());
                console.print("date: " + singleOrderToDisplay.getMmddyyyy());
                console.print("customer: " + singleOrderToDisplay.getCustomerName());
                console.print("state: " + singleOrderToDisplay.getState());
                console.print("product type: " + singleOrderToDisplay.getProductType());
                console.print("area: " + singleOrderToDisplay.getArea());
                console.print("cost of materials/square foot: " + singleOrderToDisplay.getMatCostPerSquareFoot());
                console.print("cost of labor/square foot: " + singleOrderToDisplay.getLabCostPerSquareFoot());
                console.print("tax rate: " + singleOrderToDisplay.getTaxRate());
                console.print("total cost of materials: " + singleOrderToDisplay.getMatCost());
                console.print("total cost of labor: " + singleOrderToDisplay.getLabCost());
                console.print("total tax amount: " + singleOrderToDisplay.getTax());
                console.print("total: " + singleOrderToDisplay.getTotal());
            }
        }
    }

    private void editOrder() {
        boolean keepRunning = true;
        String dateOfOrdersToDisplay = console.readString("Enter the date of the order you want to edit: (mmddyyyy):");  //add check if orders exist for this date
        ArrayList<Order> ordersForDate = dao.getOrdersForDate(dateOfOrdersToDisplay);

        do {
            console.print("Orders for " + dateOfOrdersToDisplay);
            console.print("order#\tcustomer");
            for (Order o : ordersForDate) {
                console.print(o.getOrderNumber() + "\t" + o.getCustomerName());
            }
            int orderNumberToEdit = console.readInt("Enter the order number of the order to edit or enter \"0\" to quit to main menu: ");
            if (orderNumberToEdit == 0) {
                keepRunning = false;
            } else if (!ordersForDate.contains(dao.getOrderByOrderNumber(orderNumberToEdit)) || dao.getOrderByOrderNumber(orderNumberToEdit) == null) { //what to do if the order number doesn't exist
                console.print("There is no order with that order number for " + dateOfOrdersToDisplay + ".");
            } else {
                Order orderToEdit = dao.getOrderByOrderNumber(orderNumberToEdit);
                boolean keepEditing = true;
                while (keepEditing) {
                    int editMenuChoice = console.readInt("***edit order***\n"
                            + "Select the field to edit: \n"
                            + "field (currently entered value)\n"
                            + "1. date (" + orderToEdit.getMmddyyyy() + ")\n"
                            + "2. name (" + orderToEdit.getCustomerName() + ")\n"
                            + "3. state (" + orderToEdit.getState() + ")\n"
                            + "4. product type (" + orderToEdit.getProductType() + ")\n"
                            + "5. area (" + orderToEdit.getArea() + ")\n"
                            + "6. material cost/square foot (" + orderToEdit.getMatCostPerSquareFoot() + ")\n"
                            + "7. labor cost/square foot (" + orderToEdit.getLabCostPerSquareFoot() + ")\n"
                            + "8. tax rate (" + orderToEdit.getTaxRate() + ")\n"
                            + "9. total cost of materials (" + orderToEdit.getMatCost() + ")\n"
                            + "10. total cost of labor (" + orderToEdit.getLabCost() + ")\n"
                            + "11. total tax (" + orderToEdit.getTax() + ")\n"
                            + "12. exit to main menu", 1, 12);
                    switch (editMenuChoice) {
                        case 1:
                            orderToEdit.setMmddyyyy(console.readString("Enter the date (mmddyyyy): "));
                            break;
                        case 2:
                            orderToEdit.setCustomerName(console.readString("Enter the customer name: "));
                            break;
                        case 3:
                            orderToEdit.setState(console.readString("*WARNING*\n"
                                    + "This will only change the state. The tax rate and/or total tax amount must be adjusted separately.\n"
                                    + "Enter the state (two-letter abbreviation): "));
                            break;
                        case 4:
                            orderToEdit.setProductType(console.readString("*WARNING*\n"
                                    + "This will only change the product type. Material cost, labor cost and tax and other fields must be adjusted separately.\n"
                                    + "Enter the product type: "));
                            break;
                        case 5:
                            orderToEdit.setArea(console.readDouble("*WARNING*\n"
                                    + "Changing the area of the job may change the total material and labor costs.\n"
                                    + "Enter the area (square feet.): "));
                            orderToEdit.setMatCost(orderToEdit.getArea() * orderToEdit.getMatCostPerSquareFoot());
                            orderToEdit.setLabCost(orderToEdit.getArea() * orderToEdit.getLabCostPerSquareFoot());
                            orderToEdit.setTax((orderToEdit.getMatCost() + orderToEdit.getLabCost()) * orderToEdit.getTaxRate());
                            orderToEdit.setTotal(orderToEdit.getMatCost() + orderToEdit.getLabCost() + orderToEdit.getTax());
                            break;
                        case 6:
                            orderToEdit.setMatCostPerSquareFoot(console.readDouble("*WARNING*\n"
                                    + "Changing the cost of materials/square foot may change the total material costs.\n"
                                    + "Enter the material cost/square foot: "));
                            orderToEdit.setMatCost(orderToEdit.getArea() * orderToEdit.getMatCostPerSquareFoot());
                            orderToEdit.setTax((orderToEdit.getMatCost() + orderToEdit.getLabCost()) * orderToEdit.getTaxRate());
                            orderToEdit.setTotal(orderToEdit.getMatCost() + orderToEdit.getLabCost() + orderToEdit.getTax());
                            break;
                        case 7:
                            orderToEdit.setLabCostPerSquareFoot(console.readDouble("*WARNING*\n"
                                    + "Changing the cost of labor/square foot may change the total labor costs.\n"
                                    + "Enter the labor cost/square foot: "));
                            orderToEdit.setLabCost(orderToEdit.getArea() * orderToEdit.getLabCostPerSquareFoot());
                            orderToEdit.setTax((orderToEdit.getMatCost() + orderToEdit.getLabCost()) * orderToEdit.getTaxRate());
                            orderToEdit.setTotal(orderToEdit.getMatCost() + orderToEdit.getLabCost() + orderToEdit.getTax());
                            break;
                        case 8:
                            orderToEdit.setTaxRate(console.readDouble("*WARNING*\n"
                                    + "Changing the tax rate will override the assigned tax rate for the state and may change the total tax amount.\n"
                                    + "Enter the tax rate (e.g. enter \"0.07\" for 7%: "));
                            orderToEdit.setTax((orderToEdit.getLabCost() + orderToEdit.getMatCost()) * orderToEdit.getTaxRate());
                            orderToEdit.setTotal(orderToEdit.getMatCost() + orderToEdit.getLabCost() + orderToEdit.getTax());
                            break;
                        case 9:
                            orderToEdit.setMatCost(console.readDouble("*WARNING*\n"
                                    + "Changing the total cost of materials will override the cost of materials per square foot and may change the total tax amount.\n"
                                    + "Enter the total cost of materials: "));
                            orderToEdit.setTax((orderToEdit.getMatCost() + orderToEdit.getLabCost()) * orderToEdit.getTaxRate());
                            orderToEdit.setTotal(orderToEdit.getMatCost() + orderToEdit.getLabCost() + orderToEdit.getTax());
                            break;
                        case 10:
                            orderToEdit.setLabCost(console.readDouble("*WARNING*\n"
                                    + "Changing the total cost of labor will override the cost of labor per square foot and may change the total tax amount.\n"
                                    + "Enter the total cost of labor: "));
                            orderToEdit.setTax((orderToEdit.getMatCost() + orderToEdit.getLabCost()) * orderToEdit.getTaxRate());
                            orderToEdit.setTotal(orderToEdit.getMatCost() + orderToEdit.getLabCost() + orderToEdit.getTax());
                            break;
                        case 11:
                            orderToEdit.setTax(console.readDouble("Enter the total tax amount:"));
                            orderToEdit.setTotal(orderToEdit.getMatCost() + orderToEdit.getLabCost() + orderToEdit.getTax());
                            break;
                        case 12:
                            keepRunning = false;
                            keepEditing = false;
                            break;
                        default:
                            console.print("Invalid selection. Returning to main menu.");
                            keepRunning = false;
                            keepEditing = false;
                            break;
                    }
                }
            }
        } while (keepRunning);
    }

    private void deleteOrder() {
        boolean keepRunning = true;
        String dateOfOrdersToDisplay = console.readString("Enter the date of the order to delete: (mmddyyyy):");  //add check if orders exist for this date
        ArrayList<Order> ordersForDate = dao.getOrdersForDate(dateOfOrdersToDisplay);
        while (keepRunning) {
            console.print("Orders for " + dateOfOrdersToDisplay);
            console.print("order#\tcustomer");
            for (Order o : ordersForDate) {
                console.print(o.getOrderNumber() + "\t" + o.getCustomerName());
            }
            int orderNumberToDelete = console.readInt("Enter the order number of the order to delete or enter \"0\" to quit: ");
            if (orderNumberToDelete == 0) {
                keepRunning = false;
            } else if (!ordersForDate.contains(dao.getOrderByOrderNumber(orderNumberToDelete)) || dao.getOrderByOrderNumber(orderNumberToDelete) == null) {
                console.print("There is no order with that order number for " + dateOfOrdersToDisplay + ".");
            } else {
                Order orderToDelete = dao.getOrderByOrderNumber(orderNumberToDelete);
                console.print("order #: " + orderToDelete.getOrderNumber());
                console.print("date: " + orderToDelete.getMmddyyyy());;
                console.print("customer: " + orderToDelete.getCustomerName());
                console.print("state: " + orderToDelete.getState());
                console.print("product type: " + orderToDelete.getProductType());
                console.print("total: " + orderToDelete.getTotal());

                int agreeToDelete = console.readInt("1. confirm--proceed with deletion\n"
                        + "2. cancel--return to list of orders)\n", 1, 2);
                if (agreeToDelete == 1) {
                    dao.deleteOrder(orderNumberToDelete);
                }
            }
        }
    }

    private void writeFiles() throws IOException {
        dao.writeFiles();
    }
}
